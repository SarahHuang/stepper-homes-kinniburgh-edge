/**
 * Adobe Edge: symbol definitions
 */
(function($, Edge, compId){
//images folder
var im='images/';

var fonts = {};
   fonts['']='@font-face {  font-family: \"Trade Gothic\";  src: url(\"../font/Trade Gothic.ttf\") format(\"truetype\");}';
   fonts['\'Trade Gothic\'']='<link rel=\"stylesheet\" href=\"css/font.css\" type=\"text/css\" />';


var resources = [
];
var symbols = {
"stage": {
   version: "1.5.0",
   minimumCompatibleVersion: "1.5.0",
   build: "1.5.0.217",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
         dom: [
         {
            id:'loading',
            type:'image',
            rect:['895px','476px','128px','128px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"loading.gif",'0px','0px']
         },
         {
            id:'webContainer',
            type:'rect',
            rect:['0px','0px','1920px','1080px','auto','auto'],
            fill:["rgba(192,192,192,0.0859)"],
            stroke:[0,"rgb(0, 0, 0)","none"],
            c:[
            {
               id:'webContent',
               type:'rect',
               rect:['0px','0px','1920px','1080px','auto','auto'],
               fill:["rgba(192,192,192,0)"],
               stroke:[0,"rgb(0, 0, 0)","none"]
            },
            {
               id:'close',
               type:'rect',
               rect:['1109px','2px','808px','60px','auto','auto'],
               borderRadius:["10px 10px","10px 10px","10px 10px","10px 10px"],
               fill:["rgba(20,20,20,1.00)"],
               stroke:[0,"rgb(0, 0, 0)","none"],
               c:[
               {
                  id:'txtClose',
                  type:'text',
                  rect:['39px','5px','734px','44px','auto','auto'],
                  text:"PRESS HERE TO RETURN",
                  align:"center",
                  font:['Trade Gothic',40,"rgba(255,255,255,1.00)","normal","none",""]
               },
               {
                  id:'close_edge',
                  type:'rect',
                  rect:['auto','-3px','815px','65px','-2px','auto'],
                  borderRadius:["10px 10px","10px 10px","10px 10px","10px 10px"],
                  fill:["rgba(255,255,255,0.00)"],
                  stroke:[0,"rgb(0, 0, 0)","none"]
               }]
            }]
         },
         {
            id:'community',
            display:'none',
            type:'rect',
            rect:['0','0','auto','auto','auto','auto']
         },
         {
            id:'brochure',
            display:'none',
            type:'rect',
            rect:['0','0','auto','auto','auto','auto']
         },
         {
            id:'lotMap',
            display:'none',
            type:'rect',
            rect:['0','-90','auto','auto','auto','auto']
         },
         {
            id:'modelSelection',
            type:'rect',
            rect:['0px','0px','auto','auto','auto','auto']
         }],
         symbolInstances: [
         {
            id:'lotMap',
            symbolName:'lotMap'
         },
         {
            id:'modelSelection',
            symbolName:'modelSelection'
         },
         {
            id:'brochure',
            symbolName:'brochure'
         },
         {
            id:'community',
            symbolName:'community'
         }
         ]
      },
   states: {
      "Base State": {
         "${_webContainer}": [
            ["color", "background-color", 'rgba(238,239,239,1.00)'],
            ["style", "height", '1080px'],
            ["style", "top", '0px'],
            ["style", "display", 'block'],
            ["style", "opacity", '0'],
            ["style", "left", '0px'],
            ["style", "width", '1920px']
         ],
         "${_brochure}": [
            ["style", "display", 'none'],
            ["style", "opacity", '0']
         ],
         "${_webContent}": [
            ["style", "height", '1080px']
         ],
         "${_modelSelection}": [
            ["style", "left", '0px'],
            ["style", "opacity", '0']
         ],
         "${_clear}": [
            ["style", "text-align", '']
         ],
         "${_loading}": [
            ["style", "top", '476px'],
            ["style", "height", '128px'],
            ["style", "left", '895px'],
            ["style", "width", '128px']
         ],
         "${_close}": [
            ["color", "background-color", 'rgba(20,20,20,1.00)'],
            ["style", "border-top-left-radius", [10,10], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "border-bottom-right-radius", [10,10], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "left", '1109px'],
            ["style", "width", '808px'],
            ["style", "top", '2px'],
            ["style", "border-bottom-left-radius", [10,10], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "height", '60px'],
            ["style", "border-top-right-radius", [10,10], {valueTemplate:'@@0@@px @@1@@px'} ]
         ],
         "${_txtClose}": [
            ["style", "top", '5px'],
            ["style", "font-size", '40px'],
            ["style", "text-align", 'center'],
            ["color", "color", 'rgba(255,255,255,1.00)'],
            ["style", "height", '44px'],
            ["style", "font-family", 'Trade Gothic'],
            ["style", "left", '39px'],
            ["style", "width", '734px']
         ],
         "${_sendText}": [
            ["style", "text-align", '']
         ],
         "${_Stage}": [
            ["color", "background-color", 'rgba(30,30,30,1.00)'],
            ["style", "width", '1920px'],
            ["style", "height", '1080px'],
            ["style", "overflow", 'hidden']
         ],
         "${_close_edge}": [
            ["color", "background-color", 'rgba(255,255,255,0.00)'],
            ["style", "border-top-left-radius", [10,10], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "border-bottom-right-radius", [10,10], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "right", '-2px'],
            ["style", "left", 'auto'],
            ["style", "width", '815px'],
            ["style", "top", '-3px'],
            ["style", "border-bottom-left-radius", [10,10], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "height", '65px'],
            ["style", "border-top-right-radius", [10,10], {valueTemplate:'@@0@@px @@1@@px'} ]
         ],
         "${_lotMap}": [
            ["style", "display", 'none'],
            ["style", "opacity", '0']
         ],
         "${_community}": [
            ["style", "display", 'none'],
            ["style", "opacity", '0']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 4625,
         autoPlay: false,
         labels: {
            "fade-in-web": 0,
            "fade-out-web": 500,
            "fade-in-selection": 1000,
            "fade-out-selection": 1500,
            "fade-in-lotMap": 2000,
            "fade-out-lotMap": 2500,
            "fade-in-brochure": 3000,
            "fade-out-brochure": 3500,
            "fade-in-community": 3875,
            "fade-out-community": 4375
         },
         timeline: [
            { id: "eid103", tween: [ "style", "${_lotMap}", "opacity", '1', { fromValue: '0.000000'}], position: 2000, duration: 250 },
            { id: "eid105", tween: [ "style", "${_lotMap}", "opacity", '0', { fromValue: '1'}], position: 2500, duration: 250 },
            { id: "eid97", tween: [ "style", "${_webContainer}", "opacity", '1', { fromValue: '0'}], position: 0, duration: 250 },
            { id: "eid98", tween: [ "style", "${_webContainer}", "opacity", '0', { fromValue: '1'}], position: 500, duration: 250 },
            { id: "eid96", tween: [ "color", "${_webContainer}", "background-color", 'rgba(238,239,239,1.00)', { animationColorSpace: 'RGB', valueTemplate: undefined, fromValue: 'rgba(238,239,239,1.00)'}], position: 0, duration: 0 },
            { id: "eid134", tween: [ "style", "${_brochure}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid113", tween: [ "style", "${_brochure}", "opacity", '1', { fromValue: '0.000000'}], position: 3000, duration: 250 },
            { id: "eid115", tween: [ "style", "${_brochure}", "opacity", '0', { fromValue: '1'}], position: 3500, duration: 250 },
            { id: "eid133", tween: [ "style", "${_lotMap}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid99", tween: [ "style", "${_webContainer}", "display", 'block', { fromValue: 'block'}], position: 0, duration: 0 },
            { id: "eid100", tween: [ "style", "${_webContainer}", "display", 'none', { fromValue: 'block'}], position: 750, duration: 0 },
            { id: "eid135", tween: [ "style", "${_community}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid108", tween: [ "style", "${_modelSelection}", "opacity", '1', { fromValue: '0.000000'}], position: 1000, duration: 250 },
            { id: "eid110", tween: [ "style", "${_modelSelection}", "opacity", '0', { fromValue: '1'}], position: 1500, duration: 250 },
            { id: "eid138", tween: [ "style", "${_community}", "opacity", '1', { fromValue: '0'}], position: 3875, duration: 250 },
            { id: "eid143", tween: [ "style", "${_community}", "opacity", '0', { fromValue: '1'}], position: 4375, duration: 250 }         ]
      }
   }
},
"zoomIn": {
   version: "1.5.0",
   minimumCompatibleVersion: "1.5.0",
   build: "1.5.0.217",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      rect: ['0px','0px','114px','46px','auto','auto'],
      borderRadius: ['10px','10px','10px','10px'],
      id: 'zoomRect',
      stroke: [0,'rgb(0, 0, 0)','none'],
      type: 'rect',
      fill: ['rgba(181,18,27,1.00)'],
      c: [
      {
         font: ['Arial, Helvetica, sans-serif',60,'rgba(0,0,0,1)','700','none','normal'],
         type: 'text',
         id: 'zoom',
         text: '+',
         align: 'left',
         rect: ['41px','-14px','53px','59px','auto','auto']
      }]
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_zoom}": [
            ["style", "top", '-14px'],
            ["style", "width", '53px'],
            ["style", "height", '59px'],
            ["style", "font-weight", '700'],
            ["style", "left", '41px'],
            ["style", "font-size", '60px']
         ],
         "${_zoomRect}": [
            ["style", "top", '0px'],
            ["style", "left", '0px'],
            ["color", "background-color", 'rgba(181,18,27,1.00)']
         ],
         "${symbolSelector}": [
            ["style", "height", '46px'],
            ["style", "width", '118px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 0,
         autoPlay: false,
         timeline: [
         ]
      }
   }
},
"auth": {
   version: "1.5.0",
   minimumCompatibleVersion: "1.5.0",
   build: "1.5.0.217",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      type: 'rect',
      id: 'adminBox',
      stroke: [0,'rgb(0, 0, 0)','none'],
      rect: ['6px','0px','1805px','94px','auto','auto'],
      fill: ['rgba(192,192,192,0.0859)']
   },
   {
      type: 'rect',
      id: 'keyBox',
      stroke: [0,'rgb(0, 0, 0)','none'],
      rect: ['1027px','0px','784px','94px','auto','auto'],
      fill: ['rgba(192,192,192,0.0898)'],
      c: [
      {
         rect: ['11px','13px','68px','68px','auto','auto'],
         fill: ['rgba(181,18,27,1.00)'],
         id: 'key1',
         stroke: [0,'rgb(0, 0, 0)','none'],
         type: 'rect',
         boxShadow: ['',3,3,3,0,'rgba(0,0,0,0.65)'],
         c: [
         {
            rect: ['16px','0px','39px','68px','auto','auto'],
            id: 'text1',
            text: '1',
            font: ['Arial, Helvetica, sans-serif',60,'rgba(0,0,0,1)','normal','none',''],
            type: 'text'
         }]
      },
      {
         rect: ['87px','13px','68px','68px','auto','auto'],
         fill: ['rgba(181,18,27,1.00)'],
         id: 'key2',
         stroke: [0,'rgb(0, 0, 0)','none'],
         type: 'rect',
         boxShadow: ['',3,3,3,0,'rgba(0,0,0,0.65)'],
         c: [
         {
            rect: ['16px','0px','39px','68px','auto','auto'],
            id: 'text2',
            text: '2',
            font: ['Arial, Helvetica, sans-serif',60,'rgba(0,0,0,1)','normal','none',''],
            type: 'text'
         }]
      },
      {
         rect: ['163px','13px','68px','68px','auto','auto'],
         fill: ['rgba(181,18,27,1.00)'],
         id: 'key3',
         stroke: [0,'rgb(0, 0, 0)','none'],
         type: 'rect',
         boxShadow: ['',3,3,3,0,'rgba(0,0,0,0.65)'],
         c: [
         {
            rect: ['16px','0px','39px','68px','auto','auto'],
            id: 'text3',
            text: '3',
            font: ['Arial, Helvetica, sans-serif',60,'rgba(0,0,0,1)','normal','none',''],
            type: 'text'
         }]
      },
      {
         rect: ['239px','13px','68px','68px','auto','auto'],
         fill: ['rgba(181,18,27,1.00)'],
         id: 'key4',
         stroke: [0,'rgb(0, 0, 0)','none'],
         type: 'rect',
         boxShadow: ['',3,3,3,0,'rgba(0,0,0,0.65)'],
         c: [
         {
            rect: ['16px','0px','39px','68px','auto','auto'],
            id: 'text4',
            text: '4',
            font: ['Arial, Helvetica, sans-serif',60,'rgba(0,0,0,1)','normal','none',''],
            type: 'text'
         }]
      },
      {
         rect: ['315px','13px','68px','68px','auto','auto'],
         fill: ['rgba(181,18,27,1.00)'],
         id: 'key5',
         stroke: [0,'rgb(0, 0, 0)','none'],
         type: 'rect',
         boxShadow: ['',3,3,3,0,'rgba(0,0,0,0.65)'],
         c: [
         {
            rect: ['16px','0px','39px','68px','auto','auto'],
            id: 'text5',
            text: '5',
            font: ['Arial, Helvetica, sans-serif',60,'rgba(0,0,0,1)','normal','none',''],
            type: 'text'
         }]
      },
      {
         rect: ['392px','13px','68px','68px','auto','auto'],
         fill: ['rgba(181,18,27,1.00)'],
         id: 'key6',
         stroke: [0,'rgb(0, 0, 0)','none'],
         type: 'rect',
         boxShadow: ['',3,3,3,0,'rgba(0,0,0,0.65)'],
         c: [
         {
            rect: ['16px','0px','39px','68px','auto','auto'],
            id: 'text6',
            text: '6',
            font: ['Arial, Helvetica, sans-serif',60,'rgba(0,0,0,1)','normal','none',''],
            type: 'text'
         }]
      },
      {
         rect: ['469px','13px','68px','68px','auto','auto'],
         fill: ['rgba(181,18,27,1.00)'],
         id: 'key7',
         stroke: [0,'rgb(0, 0, 0)','none'],
         type: 'rect',
         boxShadow: ['',3,3,3,0,'rgba(0,0,0,0.65)'],
         c: [
         {
            rect: ['16px','0px','39px','68px','auto','auto'],
            id: 'text7',
            text: '7',
            font: ['Arial, Helvetica, sans-serif',60,'rgba(0,0,0,1)','normal','none',''],
            type: 'text'
         }]
      },
      {
         rect: ['547px','13px','68px','68px','auto','auto'],
         fill: ['rgba(181,18,27,1.00)'],
         id: 'key8',
         stroke: [0,'rgb(0, 0, 0)','none'],
         type: 'rect',
         boxShadow: ['',3,3,3,0,'rgba(0,0,0,0.65)'],
         c: [
         {
            rect: ['16px','0px','39px','68px','auto','auto'],
            id: 'text8',
            text: '8',
            font: ['Arial, Helvetica, sans-serif',60,'rgba(0,0,0,1)','normal','none',''],
            type: 'text'
         }]
      },
      {
         rect: ['626px','13px','68px','68px','auto','auto'],
         fill: ['rgba(181,18,27,1.00)'],
         id: 'key9',
         stroke: [0,'rgb(0, 0, 0)','none'],
         type: 'rect',
         boxShadow: ['',3,3,3,0,'rgba(0,0,0,0.65)'],
         c: [
         {
            rect: ['16px','0px','39px','68px','auto','auto'],
            id: 'text9',
            text: '9',
            font: ['Arial, Helvetica, sans-serif',60,'rgba(0,0,0,1)','normal','none',''],
            type: 'text'
         }]
      },
      {
         rect: ['702px','13px','68px','68px','auto','auto'],
         fill: ['rgba(181,18,27,1.00)'],
         id: 'key0',
         stroke: [0,'rgb(0, 0, 0)','none'],
         type: 'rect',
         boxShadow: ['',3,3,3,0,'rgba(0,0,0,0.65)'],
         c: [
         {
            rect: ['16px','0px','39px','68px','auto','auto'],
            id: 'text0',
            text: '0',
            font: ['Arial, Helvetica, sans-serif',60,'rgba(0,0,0,1)','normal','none',''],
            type: 'text'
         }]
      }]
   },
   {
      type: 'rect',
      id: 'authClick',
      stroke: [0,'rgba(0,0,0,1)','none'],
      rect: ['1811px','0px','109px','94px','auto','auto'],
      fill: ['rgba(255,255,255,0.00)'],
      c: [
      {
         id: 'auth-key',
         type: 'image',
         rect: ['-5px','-13px','120px','120px','auto','auto'],
         fill: ['rgba(0,0,0,0)','images/auth-key.png','0px','0px']
      }]
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_text9}": [
            ["style", "top", '0px'],
            ["style", "height", '68px'],
            ["style", "font-size", '60px'],
            ["style", "left", '16px'],
            ["style", "width", '39px']
         ],
         "${_authClick}": [
            ["style", "top", '0px'],
            ["style", "height", '94px'],
            ["color", "background-color", 'rgba(255,255,255,0.00)'],
            ["style", "left", '1811px'],
            ["style", "width", '109px']
         ],
         "${_text5}": [
            ["style", "top", '0px'],
            ["style", "height", '68px'],
            ["style", "font-size", '60px'],
            ["style", "left", '16px'],
            ["style", "width", '39px']
         ],
         "${_key2}": [
            ["color", "background-color", 'rgba(181,18,27,1.00)'],
            ["subproperty", "boxShadow.blur", '3px'],
            ["style", "left", '87px'],
            ["style", "width", '68px'],
            ["style", "top", '13px'],
            ["style", "height", '68px'],
            ["subproperty", "boxShadow.offsetV", '3px'],
            ["subproperty", "boxShadow.offsetH", '3px'],
            ["subproperty", "boxShadow.color", 'rgba(0,0,0,0.648438)']
         ],
         "${_text7}": [
            ["style", "top", '0px'],
            ["style", "height", '68px'],
            ["style", "font-size", '60px'],
            ["style", "left", '16px'],
            ["style", "width", '39px']
         ],
         "${_key7}": [
            ["color", "background-color", 'rgba(181,18,27,1.00)'],
            ["subproperty", "boxShadow.blur", '3px'],
            ["style", "left", '469px'],
            ["style", "width", '68px'],
            ["style", "top", '13px'],
            ["style", "height", '68px'],
            ["subproperty", "boxShadow.offsetV", '3px'],
            ["subproperty", "boxShadow.offsetH", '3px'],
            ["subproperty", "boxShadow.color", 'rgba(0,0,0,0.648438)']
         ],
         "${_text1}": [
            ["style", "top", '0px'],
            ["style", "height", '68px'],
            ["style", "width", '39px'],
            ["style", "left", '16px'],
            ["style", "font-size", '60px']
         ],
         "${_text6}": [
            ["style", "top", '0px'],
            ["style", "height", '68px'],
            ["style", "font-size", '60px'],
            ["style", "left", '16px'],
            ["style", "width", '39px']
         ],
         "${_text4}": [
            ["style", "top", '0px'],
            ["style", "height", '68px'],
            ["style", "font-size", '60px'],
            ["style", "left", '16px'],
            ["style", "width", '39px']
         ],
         "${_adminBox}": [
            ["style", "width", '1805px']
         ],
         "${_text2}": [
            ["style", "top", '0px'],
            ["style", "height", '68px'],
            ["style", "font-size", '60px'],
            ["style", "left", '16px'],
            ["style", "width", '39px']
         ],
         "${_text0}": [
            ["style", "top", '0px'],
            ["style", "height", '68px'],
            ["style", "font-size", '60px'],
            ["style", "left", '16px'],
            ["style", "width", '39px']
         ],
         "${_key5}": [
            ["color", "background-color", 'rgba(181,18,27,1.00)'],
            ["subproperty", "boxShadow.blur", '3px'],
            ["style", "left", '315px'],
            ["style", "width", '68px'],
            ["style", "top", '13px'],
            ["style", "height", '68px'],
            ["subproperty", "boxShadow.offsetV", '3px'],
            ["subproperty", "boxShadow.offsetH", '3px'],
            ["subproperty", "boxShadow.color", 'rgba(0,0,0,0.648438)']
         ],
         "${_text8}": [
            ["style", "top", '0px'],
            ["style", "height", '68px'],
            ["style", "font-size", '60px'],
            ["style", "left", '16px'],
            ["style", "width", '39px']
         ],
         "${_key1}": [
            ["color", "background-color", 'rgba(181,18,27,1.00)'],
            ["subproperty", "boxShadow.blur", '3px'],
            ["style", "left", '11px'],
            ["style", "width", '68px'],
            ["style", "top", '13px'],
            ["style", "height", '68px'],
            ["subproperty", "boxShadow.offsetV", '3px'],
            ["subproperty", "boxShadow.offsetH", '3px'],
            ["subproperty", "boxShadow.color", 'rgba(0,0,0,0.65)']
         ],
         "${_key6}": [
            ["color", "background-color", 'rgba(181,18,27,1.00)'],
            ["subproperty", "boxShadow.blur", '3px'],
            ["style", "left", '392px'],
            ["style", "width", '68px'],
            ["style", "top", '13px'],
            ["style", "height", '68px'],
            ["subproperty", "boxShadow.offsetV", '3px'],
            ["subproperty", "boxShadow.offsetH", '3px'],
            ["subproperty", "boxShadow.color", 'rgba(0,0,0,0.648438)']
         ],
         "${_key9}": [
            ["color", "background-color", 'rgba(181,18,27,1.00)'],
            ["subproperty", "boxShadow.blur", '3px'],
            ["style", "left", '626px'],
            ["style", "width", '68px'],
            ["style", "top", '13px'],
            ["style", "height", '68px'],
            ["subproperty", "boxShadow.offsetV", '3px'],
            ["subproperty", "boxShadow.offsetH", '3px'],
            ["subproperty", "boxShadow.color", 'rgba(0,0,0,0.648438)']
         ],
         "${_key8}": [
            ["color", "background-color", 'rgba(181,18,27,1.00)'],
            ["subproperty", "boxShadow.blur", '3px'],
            ["style", "left", '547px'],
            ["style", "width", '68px'],
            ["style", "top", '13px'],
            ["style", "height", '68px'],
            ["subproperty", "boxShadow.offsetV", '3px'],
            ["subproperty", "boxShadow.offsetH", '3px'],
            ["subproperty", "boxShadow.color", 'rgba(0,0,0,0.648438)']
         ],
         "${_key3}": [
            ["color", "background-color", 'rgba(181,18,27,1.00)'],
            ["subproperty", "boxShadow.blur", '3px'],
            ["style", "left", '163px'],
            ["style", "width", '68px'],
            ["style", "top", '13px'],
            ["style", "height", '68px'],
            ["subproperty", "boxShadow.offsetV", '3px'],
            ["subproperty", "boxShadow.offsetH", '3px'],
            ["subproperty", "boxShadow.color", 'rgba(0,0,0,0.648438)']
         ],
         "${_auth-key}": [
            ["style", "left", '-5px'],
            ["style", "top", '-13px']
         ],
         "${_text3}": [
            ["style", "top", '0px'],
            ["style", "height", '68px'],
            ["style", "font-size", '60px'],
            ["style", "left", '16px'],
            ["style", "width", '39px']
         ],
         "${symbolSelector}": [
            ["style", "height", '94px'],
            ["style", "width", '1920px']
         ],
         "${_keyBox}": [
            ["style", "top", '-90px'],
            ["style", "left", '1027px'],
            ["style", "width", '784px']
         ],
         "${_key0}": [
            ["color", "background-color", 'rgba(181,18,27,1.00)'],
            ["subproperty", "boxShadow.blur", '3px'],
            ["style", "left", '702px'],
            ["style", "width", '68px'],
            ["style", "top", '13px'],
            ["style", "height", '68px'],
            ["subproperty", "boxShadow.offsetV", '3px'],
            ["subproperty", "boxShadow.offsetH", '3px'],
            ["subproperty", "boxShadow.color", 'rgba(0,0,0,0.648438)']
         ],
         "${_key4}": [
            ["color", "background-color", 'rgba(181,18,27,1.00)'],
            ["subproperty", "boxShadow.blur", '3px'],
            ["style", "left", '239px'],
            ["style", "width", '68px'],
            ["style", "top", '13px'],
            ["style", "height", '68px'],
            ["subproperty", "boxShadow.offsetV", '3px'],
            ["subproperty", "boxShadow.offsetH", '3px'],
            ["subproperty", "boxShadow.color", 'rgba(0,0,0,0.648438)']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 1500,
         autoPlay: false,
         labels: {
            "keys-in": 0,
            "keys-out": 1000
         },
         timeline: [
            { id: "eid10", tween: [ "style", "${_keyBox}", "top", '0px', { fromValue: '-90px'}], position: 0, duration: 500, easing: "easeOutBounce" },
            { id: "eid17", tween: [ "style", "${_keyBox}", "top", '-90px', { fromValue: '0px'}], position: 1000, duration: 500, easing: "easeOutQuart" }         ]
      }
   }
},
"zoomOut": {
   version: "1.5.0",
   minimumCompatibleVersion: "1.5.0",
   build: "1.5.0.217",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      rect: ['0px','0px','114px','46px','auto','auto'],
      borderRadius: ['10px','10px','10px','10px'],
      id: 'zoomRect',
      stroke: [0,'rgb(0, 0, 0)','none'],
      type: 'rect',
      fill: ['rgba(181,18,27,1.00)'],
      c: [
      {
         font: ['Arial, Helvetica, sans-serif',95,'rgba(0,0,0,1)','700','none','normal'],
         type: 'text',
         id: 'zoom',
         text: '-',
         align: 'left',
         rect: ['39px','-41px','53px','59px','auto','auto']
      }]
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_zoom}": [
            ["style", "top", '-41px'],
            ["style", "font-size", '95px'],
            ["style", "height", '59px'],
            ["style", "font-weight", '700'],
            ["style", "left", '39px'],
            ["style", "width", '53px']
         ],
         "${_zoomRect}": [
            ["style", "top", '0px'],
            ["style", "left", '0px'],
            ["color", "background-color", 'rgba(181,18,27,1.00)']
         ],
         "${symbolSelector}": [
            ["style", "height", '46px'],
            ["style", "width", '118px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 0,
         autoPlay: false,
         timeline: [
         ]
      }
   }
},
"lotMap": {
   version: "1.5.0",
   minimumCompatibleVersion: "1.5.0",
   build: "1.5.0.217",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      id: 'background',
      type: 'image',
      rect: ['0px','90px','1920px','1080px','auto','auto'],
      fill: ['rgba(0,0,0,0)','images/bg-lots.jpg','0px','0px']
   },
   {
      rect: ['482px','204px','1100px','866px','auto','auto'],
      borderRadius: ['20px 20px','20px 20px','0px','20px 20px'],
      opacity: 1,
      id: 'lotMap',
      stroke: [0,'rgba(0,0,0,1)','none'],
      type: 'rect',
      fill: ['rgba(192,192,192,0.09)'],
      c: [
      {
         id: 'background',
         type: 'image',
         rect: ['0px','90px','1920px','1080px','auto','auto'],
         fill: ['rgba(0,0,0,0)','images/bg-lots.jpg','0px','0px']
      },
      {
         rect: ['482px','204px','1100px','866px','auto','auto'],
         borderRadius: ['20px 20px','20px 20px','0px','20px 20px'],
         id: 'lotMap',
         stroke: [0,'rgba(0,0,0,1)','none'],
         type: 'rect',
         fill: ['rgba(192,192,192,0.09)']
      },
      {
         id: 'auth',
         type: 'rect',
         rect: ['0px','90px','auto','auto','auto','auto']
      },
      {
         id: 'controls',
         type: 'group',
         rect: ['16px','98px','612px','77px','auto','auto'],
         c: [
         {
            rect: ['423px','20px','114px','46px','auto','auto'],
            borderRadius: ['10px','10px','10px','10px'],
            id: 'clearRect',
            stroke: [0,'rgb(0, 0, 0)','none'],
            type: 'rect',
            fill: ['rgba(181,18,27,1.00)'],
            c: [
            {
               font: ['Arial, Helvetica, sans-serif',20,'rgba(0,0,0,1)','normal','none','normal'],
               type: 'text',
               id: 'clear',
               text: 'Reset Map',
               align: 'left',
               rect: ['8px','11px','97px','33px','auto','auto']
            }]
         },
         {
            type: 'rect',
            id: 'siteButton',
            stroke: [0,'rgb(0, 0, 0)','none'],
            rect: ['0px','0px','150px','77px','auto','auto'],
            fill: ['rgba(192,192,192,0.00)']
         },
         {
            id: 'zoomOut',
            type: 'rect',
            rect: ['307px','20px','auto','auto','auto','auto'],
            transform: [[0,0],[],[],['0.966']]
         },
         {
            id: 'zoomIn',
            type: 'rect',
            rect: ['191px','20px','auto','auto','auto','auto']
         }]
      }]
   },
   {
      id: 'auth',
      type: 'rect',
      rect: ['0px','90px','auto','auto','auto','auto']
   },
   {
      id: 'controls',
      type: 'group',
      rect: ['16px','98px','612px','77px','auto','auto'],
      c: [
      {
         rect: ['423px','20px','114px','46px','auto','auto'],
         borderRadius: ['10px','10px','10px','10px'],
         id: 'clearRect',
         stroke: [0,'rgb(0, 0, 0)','none'],
         type: 'rect',
         fill: ['rgba(193,216,47,1.00)'],
         c: [
         {
            font: ['Trade Gothic',20,'rgba(0,0,0,1)','normal','none','normal'],
            type: 'text',
            id: 'clear',
            text: 'Reset Map',
            align: 'center',
            rect: ['8px','11px','97px','33px','auto','auto']
         }]
      },
      {
         id: 'zoomOut',
         type: 'rect',
         rect: ['307px','20px','auto','auto','auto','auto'],
         transform: [[0,0],[],[],['0.966']]
      },
      {
         id: 'zoomIn',
         type: 'rect',
         rect: ['191px','20px','auto','auto','auto','auto']
      }]
   },
   {
      type: 'rect',
      id: 'back',
      stroke: [0,'rgba(0,0,0,1)','none'],
      rect: ['0px','90px','132px','101px','auto','auto'],
      fill: ['rgba(255,255,255,0.00)']
   },
   {
      type: 'rect',
      borderRadius: ['10px','10px','10px','10px'],
      rect: ['698px','1088px','97px','75px','auto','auto'],
      id: 'clickA',
      stroke: [0,'rgb(0, 0, 0)','none'],
      display: 'none',
      fill: ['rgba(181,18,27,1.00)'],
      c: [
      {
         font: ['Trade Gothic',24,'rgba(0,0,0,1)','normal','none',''],
         type: 'text',
         display: 'none',
         id: 'textA',
         text: 'Phase 16',
         align: 'center',
         rect: ['13px','10px','71px','55px','auto','auto']
      }]
   },
   {
      type: 'rect',
      borderRadius: ['10px','10px','10px','10px'],
      rect: ['805px','1088px','97px','75px','auto','auto'],
      id: 'clickB',
      stroke: [0,'rgb(0, 0, 0)','none'],
      display: 'none',
      fill: ['rgba(181,18,27,1.00)'],
      c: [
      {
         font: ['Trade Gothic',24,'rgba(0,0,0,1)','normal','none',''],
         type: 'text',
         display: 'none',
         id: 'textB',
         text: 'Phase <br>8',
         align: 'center',
         rect: ['13px','10px','71px','55px','auto','auto']
      }]
   },
   {
      rect: ['911px','1088px','97px','75px','auto','auto'],
      borderRadius: ['10px','10px','10px','10px'],
      id: 'clickC',
      stroke: [0,'rgb(0, 0, 0)','none'],
      type: 'rect',
      fill: ['rgba(181,18,27,1.00)'],
      c: [
      {
         font: ['Trade Gothic',24,'rgba(0,0,0,1)','normal','none',''],
         type: 'text',
         id: 'textC',
         text: 'Phase <br>1',
         align: 'center',
         rect: ['13px','10px','71px','55px','auto','auto']
      }]
   },
   {
      type: 'rect',
      borderRadius: ['10px','10px','10px','10px'],
      rect: ['1019px','1088px','97px','75px','auto','auto'],
      id: 'clickD',
      stroke: [0,'rgb(0, 0, 0)','none'],
      display: 'none',
      fill: ['rgba(181,18,27,1.00)'],
      c: [
      {
         font: ['Trade Gothic',24,'rgba(0,0,0,1)','normal','none',''],
         type: 'text',
         display: 'none',
         id: 'textD',
         text: 'Phase <br>15',
         align: 'center',
         rect: ['13px','10px','71px','55px','auto','auto']
      }]
   },
   {
      type: 'rect',
      borderRadius: ['10px','10px','10px','10px'],
      rect: ['1124px','1088px','97px','75px','auto','auto'],
      id: 'clickE',
      stroke: [0,'rgb(0, 0, 0)','none'],
      display: 'none',
      fill: ['rgba(181,18,27,1.00)'],
      c: [
      {
         font: ['Trade Gothic',24,'rgba(0,0,0,1)','normal','none',''],
         type: 'text',
         display: 'none',
         id: 'textE',
         text: 'Phase 24',
         align: 'center',
         rect: ['13px','10px','71px','55px','auto','auto']
      }]
   },
   {
      type: 'rect',
      rect: ['0px','0px','1920px','1170px','auto','auto'],
      id: 'miniMapWindow',
      stroke: [0,'rgba(0,0,0,1)','none'],
      display: 'none',
      fill: ['rgba(255,255,255,0.00)'],
      c: [
      {
         type: 'rect',
         id: 'miniMapBlack',
         stroke: [0,'rgba(0,0,0,1)','none'],
         rect: ['0px','90px','1930px','1081px','auto','auto'],
         fill: ['rgba(255,255,255,1.00)']
      },
      {
         type: 'rect',
         id: 'miniMap',
         stroke: [0,'rgb(0, 0, 0)','none'],
         rect: ['390px','139px','1150px','1000px','auto','auto'],
         fill: ['rgba(255,255,255,0.00)']
      },
      {
         rect: ['1617px','1039px','270px','100px','auto','auto'],
         borderRadius: ['10px','10px','10px','10px'],
         id: 'miniMapClose',
         stroke: [0,'rgb(0, 0, 0)','none'],
         type: 'rect',
         fill: ['rgba(181,18,27,1.00)'],
         c: [
         {
            font: ['\'Trade Gothic\'',40,'rgba(255,255,255,1.00)','normal','none','normal'],
            type: 'text',
            id: 'miniMapCloseText',
            text: 'Close Map',
            align: 'center',
            rect: ['10px','25px','250px','50px','auto','auto']
         }]
      }]
   }],
   symbolInstances: [
   {
      id: 'zoomIn',
      symbolName: 'zoomIn'
   },
   {
      id: 'zoomOut',
      symbolName: 'zoomOut'
   },
   {
      id: 'auth',
      symbolName: 'auth'
   }   ]
   },
   states: {
      "Base State": {
         "${_miniMapClose}": [
            ["style", "top", '1039px'],
            ["color", "background-color", 'rgba(181,18,27,1.00)'],
            ["style", "height", '100px'],
            ["style", "display", 'block'],
            ["style", "opacity", '1'],
            ["style", "left", '1617px'],
            ["style", "width", '270px']
         ],
         "${_clear}": [
            ["style", "top", '11px'],
            ["style", "text-align", 'center'],
            ["style", "font-size", '20px'],
            ["style", "height", '33px'],
            ["style", "font-family", 'Trade Gothic'],
            ["style", "left", '8px'],
            ["style", "width", '97px']
         ],
         "${_miniMapBlack}": [
            ["style", "height", '1081px'],
            ["style", "top", '90px'],
            ["color", "background-color", 'rgba(255,255,255,1.00)']
         ],
         "${_textD}": [
            ["style", "top", '10px'],
            ["style", "text-align", 'center'],
            ["style", "height", '55px'],
            ["style", "display", 'none'],
            ["style", "font-family", 'Trade Gothic'],
            ["style", "left", '13px'],
            ["style", "width", '71px']
         ],
         "${symbolSelector}": [
            ["style", "height", '1170px'],
            ["style", "width", '1930px']
         ],
         "${_textB}": [
            ["style", "top", '10px'],
            ["style", "text-align", 'center'],
            ["style", "height", '55px'],
            ["style", "display", 'none'],
            ["style", "font-family", 'Trade Gothic'],
            ["style", "left", '13px'],
            ["style", "width", '71px']
         ],
         "${_textC}": [
            ["style", "top", '10px'],
            ["style", "text-align", 'center'],
            ["style", "height", '55px'],
            ["style", "font-family", 'Trade Gothic'],
            ["style", "left", '13px'],
            ["style", "width", '71px']
         ],
         "${_miniMapWindow}": [
            ["style", "display", 'none'],
            ["style", "opacity", '0'],
            ["style", "height", '1170px'],
            ["color", "background-color", 'rgba(255,255,255,0.00)']
         ],
         "${_zoomOut}": [
            ["style", "top", '20px'],
            ["style", "left", '307px'],
            ["transform", "scaleX", '0.9661']
         ],
         "${_zoomIn}": [
            ["style", "left", '191px'],
            ["style", "top", '20px']
         ],
         "${_miniMap}": [
            ["style", "top", '139px'],
            ["style", "height", '1000px'],
            ["color", "background-color", 'rgba(255,255,255,0.00)'],
            ["style", "left", '390px'],
            ["style", "width", '1150px']
         ],
         "${_clickD}": [
            ["style", "top", '1088px'],
            ["color", "background-color", 'rgba(181,18,27,1)'],
            ["style", "height", '75px'],
            ["style", "display", 'none'],
            ["style", "left", '1019px'],
            ["style", "width", '97px']
         ],
         "${_clickB}": [
            ["style", "top", '1088px'],
            ["color", "background-color", 'rgba(181,18,27,1)'],
            ["style", "height", '75px'],
            ["style", "display", 'none'],
            ["style", "left", '805px'],
            ["style", "width", '97px']
         ],
         "${_controls}": [
            ["style", "top", '98px'],
            ["style", "display", 'block'],
            ["style", "opacity", '1'],
            ["style", "left", '16px'],
            ["style", "width", '612px']
         ],
         "${_textA}": [
            ["style", "top", '10px'],
            ["style", "text-align", 'center'],
            ["style", "display", 'none'],
            ["style", "height", '55px'],
            ["style", "font-family", 'Trade Gothic'],
            ["style", "left", '13px'],
            ["style", "width", '71px']
         ],
         "${_background}": [
            ["style", "left", '0px'],
            ["style", "top", '90px']
         ],
         "${_textE}": [
            ["style", "top", '10px'],
            ["style", "text-align", 'center'],
            ["style", "display", 'none'],
            ["style", "height", '55px'],
            ["style", "font-family", 'Trade Gothic'],
            ["style", "left", '13px'],
            ["style", "width", '71px']
         ],
         "${_back}": [
            ["color", "background-color", 'rgba(255,255,255,0.00)'],
            ["style", "height", '101px']
         ],
         "${_clickA}": [
            ["style", "top", '1088px'],
            ["style", "display", 'none'],
            ["style", "height", '75px'],
            ["color", "background-color", 'rgba(181,18,27,1.00)'],
            ["style", "left", '698px'],
            ["style", "width", '97px']
         ],
         "${_auth}": [
            ["style", "top", '90px'],
            ["style", "left", '66px'],
            ["transform", "scaleX", '0.93097']
         ],
         "${_clearRect}": [
            ["style", "top", '20px'],
            ["style", "left", '423px'],
            ["color", "background-color", 'rgba(181,18,27,1.00)']
         ],
         "${_clickE}": [
            ["style", "top", '1088px'],
            ["style", "display", 'none'],
            ["style", "height", '75px'],
            ["color", "background-color", 'rgba(181,18,27,1)'],
            ["style", "left", '1124px'],
            ["style", "width", '97px']
         ],
         "${_clickC}": [
            ["style", "top", '1088px'],
            ["style", "height", '75px'],
            ["color", "background-color", 'rgba(181,18,27,1)'],
            ["style", "left", '911px'],
            ["style", "width", '97px']
         ],
         "${_lotMap}": [
            ["color", "background-color", 'rgba(192,192,192,0.09)'],
            ["style", "border-top-left-radius", [20,20], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "opacity", '1'],
            ["style", "left", '0px'],
            ["style", "width", '1920px'],
            ["style", "top", '198px'],
            ["style", "border-bottom-left-radius", [20,20], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "overflow", 'hidden'],
            ["style", "height", '881px'],
            ["style", "border-top-right-radius", [20,20], {valueTemplate:'@@0@@px @@1@@px'} ]
         ],
         "${_miniMapCloseText}": [
            ["style", "top", '25px'],
            ["style", "font-size", '40px'],
            ["style", "text-align", 'center'],
            ["color", "color", 'rgba(255,255,255,1)'],
            ["style", "height", '50px'],
            ["style", "font-family", '\'Trade Gothic\''],
            ["style", "left", '10px'],
            ["style", "width", '250px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 2250,
         autoPlay: false,
         labels: {
            "fade-in-lotMap": 0,
            "fade-in-miniMap": 500,
            "fade-out-miniMap": 1000,
            "fade-out-controls": 1500,
            "fade-in-controls": 2000
         },
         timeline: [
            { id: "eid228", tween: [ "style", "${_textB}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid211", tween: [ "style", "${_miniMapWindow}", "display", 'block', { fromValue: 'none'}], position: 500, duration: 0 },
            { id: "eid216", tween: [ "style", "${_miniMapWindow}", "display", 'none', { fromValue: 'block'}], position: 1250, duration: 0 },
            { id: "eid86", tween: [ "style", "${_lotMap}", "width", '1920px', { fromValue: '1920px'}], position: 0, duration: 0 },
            { id: "eid227", tween: [ "style", "${_clickB}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid95", tween: [ "style", "${_auth}", "left", '66px', { fromValue: '66px'}], position: 0, duration: 0 },
            { id: "eid204", tween: [ "style", "${_lotMap}", "top", '198px', { fromValue: '198px'}], position: 0, duration: 0 },
            { id: "eid220", tween: [ "style", "${_textA}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid38", tween: [ "style", "${_lotMap}", "left", '0px', { fromValue: '0px'}], position: 0, duration: 0 },
            { id: "eid206", tween: [ "style", "${_lotMap}", "height", '881px', { fromValue: '881px'}], position: 0, duration: 0 },
            { id: "eid88", tween: [ "style", "${_controls}", "display", 'block', { fromValue: 'block'}], position: 1500, duration: 0 },
            { id: "eid89", tween: [ "style", "${_controls}", "display", 'none', { fromValue: 'block'}], position: 1750, duration: 0 },
            { id: "eid90", tween: [ "style", "${_controls}", "display", 'block', { fromValue: 'none'}], position: 2000, duration: 0 },
            { id: "eid71", tween: [ "style", "${_controls}", "display", 'block', { fromValue: 'block'}], position: 2250, duration: 0 },
            { id: "eid221", tween: [ "style", "${_clickE}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid91", tween: [ "style", "${_controls}", "opacity", '0', { fromValue: '1'}], position: 1500, duration: 250 },
            { id: "eid92", tween: [ "style", "${_controls}", "opacity", '1', { fromValue: '0'}], position: 2000, duration: 250 },
            { id: "eid222", tween: [ "style", "${_textE}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid225", tween: [ "style", "${_clickD}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid94", tween: [ "transform", "${_auth}", "scaleX", '0.93097', { fromValue: '0.93097'}], position: 0, duration: 0 },
            { id: "eid214", tween: [ "style", "${_miniMapWindow}", "opacity", '1', { fromValue: '0'}], position: 500, duration: 250 },
            { id: "eid215", tween: [ "style", "${_miniMapWindow}", "opacity", '0', { fromValue: '1'}], position: 1000, duration: 250 },
            { id: "eid226", tween: [ "style", "${_textD}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid219", tween: [ "style", "${_clickA}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 }         ]
      }
   }
},
"modelSelection": {
   version: "1.5.0",
   minimumCompatibleVersion: "1.5.0",
   build: "1.5.0.217",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      id: 'bg-selection',
      type: 'image',
      rect: ['0px','0px','1920px','1080px','auto','auto'],
      fill: ['rgba(0,0,0,0)','images/bg-selection.jpg','0px','0px']
   },
   {
      rect: ['967px','0px','472px','199px','auto','auto'],
      id: 'lotLink',
      stroke: [0,'rgb(0, 0, 0)','none'],
      type: 'rect',
      fill: ['rgba(58,54,55,0)']
   },
   {
      rect: ['0px','0px','472px','199px','auto','auto'],
      id: 'stepperLink',
      stroke: [0,'rgb(0, 0, 0)','none'],
      type: 'rect',
      fill: ['rgba(58,54,55,0)']
   },
   {
      rect: ['1448px','0px','472px','199px','auto','auto'],
      id: 'homeAlbumLink',
      stroke: [0,'rgb(0, 0, 0)','none'],
      type: 'rect',
      fill: ['rgba(58,54,55,0)']
   },
   {
      rect: ['484px','0px','470px','199px','auto','auto'],
      id: 'communityLink',
      stroke: [0,'rgb(0, 0, 0)','none'],
      type: 'rect',
      fill: ['rgba(58,54,55,0)']
   },
   {
      rect: ['0','205','1920','812','auto','auto'],
      id: 'container',
      type: 'group',
      userClass: 'swiper-container',
      c: [
      {
         rect: ['0px','0px','1920px','812px','auto','auto'],
         userClass: 'swiper-wrapper',
         id: 'wrapper',
         stroke: [0,'rgb(0, 0, 0)','none'],
         type: 'rect',
         fill: ['rgba(181,18,27,0.00)']
      }]
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_wrapper}": [
            ["color", "background-color", 'rgba(181,18,27,0.00)'],
            ["style", "height", '812px'],
            ["style", "top", '0px']
         ],
         "${_communityLink}": [
            ["style", "left", '484px'],
            ["style", "width", '470px']
         ],
         "${_stepperLink}": [
            ["style", "width", '472px']
         ],
         "${symbolSelector}": [
            ["style", "height", '1080px'],
            ["style", "width", '1920px']
         ],
         "${_lotLink}": [
            ["style", "left", '967px'],
            ["style", "width", '472px']
         ],
         "${_bg-selection}": [
            ["style", "left", '0px'],
            ["style", "top", '0px']
         ],
         "${_homeAlbumLink}": [
            ["style", "left", '1448px'],
            ["style", "width", '472px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 0,
         autoPlay: false,
         timeline: [
         ]
      }
   }
},
"brochure": {
   version: "1.5.0",
   minimumCompatibleVersion: "1.5.0",
   build: "1.5.0.217",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      id: 'bg-brochure',
      type: 'image',
      rect: ['0px','0px','1920px','1080px','auto','auto'],
      fill: ['rgba(0,0,0,0)','images/bg-brochure.jpg','0px','0px']
   },
   {
      rect: ['0px','0px','131px','100px','auto','auto'],
      id: 'back',
      stroke: [0,'rgb(0, 0, 0)','none'],
      type: 'rect',
      fill: ['rgba(255,255,255,0)']
   },
   {
      rect: ['391px','110px','1200px','950px','auto','auto'],
      id: 'floorplans',
      stroke: [0,'rgb(0, 0, 0)','none'],
      type: 'rect',
      fill: ['rgba(181,18,27,0.00)']
   },
   {
      rect: ['0px','111px','335px','969px','auto','auto'],
      boxShadow: ['',3,3,3,0,'rgba(0,0,0,0.65)'],
      id: 'elevationFlyout',
      stroke: [0,'rgb(0, 0, 0)','none'],
      type: 'rect',
      fill: ['rgba(181,18,27,1.00)'],
      c: [
      {
         rect: ['-37px','459px','325px','325px','auto','auto'],
         borderRadius: ['50px 50px','50px 50px','50px 50px','50px 50px'],
         transform: [[],[],[],['0.22','0.22']],
         id: 'elevation0',
         stroke: [0,'rgb(0, 0, 0)','none'],
         type: 'rect',
         fill: ['rgba(0,0,0,0.00)']
      },
      {
         rect: ['48px','459px','325px','325px','auto','auto'],
         borderRadius: ['50px 50px','50px 50px','50px 50px','50px 50px'],
         transform: [[],[],[],['0.22','0.22']],
         id: 'elevation1',
         stroke: [0,'rgb(0, 0, 0)','none'],
         type: 'rect',
         fill: ['rgba(0,0,0,0.00)']
      },
      {
         rect: ['90px','459px','325px','325px','auto','auto'],
         borderRadius: ['50px 50px','50px 50px','50px 50px','50px 50px'],
         transform: [[],[],[],['0.22','0.22']],
         id: 'elevation2',
         stroke: [0,'rgb(0, 0, 0)','none'],
         type: 'rect',
         fill: ['rgba(0,0,0,0.00)']
      },
      {
         rect: ['5px','248px','325px','325px','auto','auto'],
         borderRadius: ['10px 10px','10px 10px','10px 10px','10px 10px'],
         id: 'elevationImage',
         stroke: [0,'rgb(0, 0, 0)','none'],
         type: 'rect',
         fill: ['rgba(58,54,55,1)']
      },
      {
         rect: ['0px','0px','335px','91px','auto','auto'],
         id: 'sqFtHeader',
         stroke: [0,'rgb(0, 0, 0)','none'],
         type: 'rect',
         fill: ['rgba(0,0,0,1.00)']
      },
      {
         rect: ['0px','12px','335px','66px','auto','auto'],
         font: ['Trade Gothic',54,'rgba(255,255,255,1.00)','normal','none','normal'],
         id: 'sqFt',
         text: '2206 sq.ft.',
         align: 'center',
         type: 'text'
      },
      {
         rect: ['5px','124px','325px','116px','auto','auto'],
         font: ['Trade Gothic',60,'rgba(255,255,255,1)','normal','none','normal'],
         id: 'modelName',
         text: 'Birch Falls',
         align: 'center',
         type: 'text'
      },
      {
         rect: ['0px','692px','335px','151px','auto','auto'],
         font: ['Trade Gothic',50,'rgba(255,255,255,1)','normal','none','normal'],
         id: 'elevationName',
         text: 'Craftsman Elevation',
         align: 'center',
         type: 'text'
      }]
   },
   {
      id: 'optionsBox',
      type: 'group',
      rect: ['1608px','128px','284px','932px','auto','auto'],
      c: [
      {
         rect: ['0px','123px','284px','809px','auto','auto'],
         id: 'optionsContainer',
         stroke: [0,'rgb(0, 0, 0)','none'],
         type: 'rect',
         fill: ['rgba(181,18,27,0.00)']
      },
      {
         rect: ['0px','0px','284px','116px','auto','auto'],
         font: ['Trade Gothic',48,'rgba(0,0,0,1)','700','none','normal'],
         id: 'optionHeader',
         text: 'Available Options',
         align: 'center',
         type: 'text'
      }]
   },
   {
      display: 'none',
      type: 'rect',
      rect: ['0','110','auto','auto','auto','auto'],
      id: 'emailContainer'
   },
   {
      rect: ['1789px','0px','130px','100px','auto','auto'],
      id: 'emailLink',
      stroke: [0,'rgb(0, 0, 0)','none'],
      type: 'rect',
      fill: ['rgba(58,54,55,0)']
   },
   {
      display: 'none',
      type: 'rect',
      rect: ['0','100','auto','auto','auto','auto'],
      id: 'galleryContainer'
   },
   {
      id: 'galleryLink',
      type: 'image',
      rect: ['1689px','0px','100px','100px','auto','auto'],
      fill: ['rgba(0,0,0,0)','images/icon-gallery.jpg','0px','0px']
   }],
   symbolInstances: [
   {
      id: 'galleryContainer',
      symbolName: 'galleryContainer'
   },
   {
      id: 'emailContainer',
      symbolName: 'emailContainer'
   }   ]
   },
   states: {
      "Base State": {
         "${_emailContainer}": [
            ["style", "top", '101px'],
            ["transform", "scaleY", '1.00162'],
            ["transform", "scaleX", '1.00413'],
            ["style", "opacity", '0'],
            ["style", "left", '4px'],
            ["style", "display", 'none']
         ],
         "${_elevationName}": [
            ["style", "top", '692px'],
            ["style", "font-size", '50px'],
            ["style", "font-family", 'Trade Gothic'],
            ["style", "left", '0px'],
            ["style", "width", '335px']
         ],
         "${_optionsContainer}": [
            ["style", "top", '123px'],
            ["style", "height", '809px'],
            ["color", "background-color", 'rgba(181,18,27,0.00)'],
            ["style", "left", '0px'],
            ["style", "width", '284px']
         ],
         "${_sqFtHeader}": [
            ["color", "background-color", 'rgba(0,0,0,1.00)'],
            ["style", "top", '0px'],
            ["style", "width", '335px']
         ],
         "${_bg-brochure}": [
            ["style", "left", '0px'],
            ["style", "top", '0px']
         ],
         "${symbolSelector}": [
            ["style", "height", '1080px'],
            ["style", "width", '1920px']
         ],
         "${_elevationImage}": [
            ["style", "top", '248px'],
            ["style", "border-bottom-left-radius", [10,10], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "border-top-left-radius", [10,10], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "border-bottom-right-radius", [10,10], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "height", '325px'],
            ["style", "left", '5px'],
            ["style", "border-top-right-radius", [10,10], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "width", '325px']
         ],
         "${_elevation2}": [
            ["color", "background-color", 'rgba(0,0,0,0.00)'],
            ["style", "border-top-left-radius", [50,50], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "border-bottom-right-radius", [50,50], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["transform", "scaleX", '0.22'],
            ["style", "left", '90px'],
            ["style", "width", '325px'],
            ["style", "top", '459px'],
            ["style", "border-bottom-left-radius", [50,50], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["transform", "scaleY", '0.22'],
            ["style", "height", '325px'],
            ["style", "border-top-right-radius", [50,50], {valueTemplate:'@@0@@px @@1@@px'} ]
         ],
         "${_sqFt}": [
            ["style", "top", '12px'],
            ["style", "width", '335px'],
            ["color", "color", 'rgba(255,255,255,1.00)'],
            ["style", "font-family", 'Trade Gothic'],
            ["style", "left", '0px'],
            ["style", "font-size", '54px']
         ],
         "${_modelName}": [
            ["style", "top", '124px'],
            ["style", "font-size", '60px'],
            ["style", "height", '116px'],
            ["style", "font-family", 'Trade Gothic'],
            ["style", "left", '5px'],
            ["style", "width", '325px']
         ],
         "${_elevationFlyout}": [
            ["color", "background-color", 'rgba(181,18,27,1.00)'],
            ["subproperty", "boxShadow.inset", ''],
            ["subproperty", "boxShadow.color", 'rgba(0,0,0,0.65)'],
            ["style", "left", '0px'],
            ["style", "width", '335px'],
            ["style", "height", '969px'],
            ["subproperty", "boxShadow.offsetV", '3px'],
            ["subproperty", "boxShadow.offsetH", '3px'],
            ["subproperty", "boxShadow.blur", '3px']
         ],
         "${_elevation0}": [
            ["color", "background-color", 'rgba(0,0,0,0.00)'],
            ["style", "border-top-left-radius", [50,50], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "border-bottom-right-radius", [50,50], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["transform", "scaleX", '0.22'],
            ["style", "left", '-37px'],
            ["style", "width", '325px'],
            ["style", "top", '459px'],
            ["style", "border-bottom-left-radius", [50,50], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["transform", "scaleY", '0.22'],
            ["style", "height", '325px'],
            ["style", "border-top-right-radius", [50,50], {valueTemplate:'@@0@@px @@1@@px'} ]
         ],
         "${_floorplans}": [
            ["style", "top", '110px'],
            ["style", "height", '950px'],
            ["color", "background-color", 'rgba(181,18,27,0.00)'],
            ["style", "left", '391px'],
            ["style", "width", '1200px']
         ],
         "${_optionHeader}": [
            ["style", "top", '0px'],
            ["style", "font-weight", '700'],
            ["style", "height", '116px'],
            ["style", "font-family", 'Trade Gothic'],
            ["style", "left", '0px'],
            ["style", "width", '284px']
         ],
         "${_galleryContainer}": [
            ["style", "display", 'none'],
            ["style", "opacity", '0']
         ],
         "${_emailLink}": [
            ["style", "left", '1789px'],
            ["style", "width", '130px']
         ],
         "${_optionsBox}": [
            ["style", "height", '932px'],
            ["style", "top", '128px'],
            ["style", "left", '1608px'],
            ["style", "width", '284px']
         ],
         "${_elevation1}": [
            ["color", "background-color", 'rgba(0,0,0,0.00)'],
            ["style", "border-top-left-radius", [50,50], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "border-bottom-right-radius", [50,50], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["transform", "scaleX", '0.22'],
            ["style", "left", '48px'],
            ["style", "width", '325px'],
            ["style", "top", '459px'],
            ["style", "border-bottom-left-radius", [50,50], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["transform", "scaleY", '0.22'],
            ["style", "height", '325px'],
            ["style", "border-top-right-radius", [50,50], {valueTemplate:'@@0@@px @@1@@px'} ]
         ],
         "${_galleryLink}": [
            ["style", "top", '0px'],
            ["style", "left", '1689px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 2000,
         autoPlay: false,
         labels: {
            "fade-in-email": 250,
            "fade-out-email": 750,
            "fade-in-gallery": 1250,
            "fade-out-gallery": 1750
         },
         timeline: [
            { id: "eid174", tween: [ "style", "${_galleryContainer}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid175", tween: [ "style", "${_galleryContainer}", "display", 'block', { fromValue: 'none'}], position: 1250, duration: 0 },
            { id: "eid176", tween: [ "style", "${_galleryContainer}", "display", 'none', { fromValue: 'block'}], position: 2000, duration: 0 },
            { id: "eid148", tween: [ "style", "${_emailContainer}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid150", tween: [ "style", "${_emailContainer}", "display", 'block', { fromValue: 'none'}], position: 250, duration: 0 },
            { id: "eid173", tween: [ "style", "${_emailContainer}", "display", 'none', { fromValue: 'block'}], position: 1000, duration: 0 },
            { id: "eid178", tween: [ "style", "${_galleryContainer}", "opacity", '1', { fromValue: '0'}], position: 1250, duration: 250 },
            { id: "eid179", tween: [ "style", "${_galleryContainer}", "opacity", '0', { fromValue: '1'}], position: 1750, duration: 250 },
            { id: "eid152", tween: [ "style", "${_emailContainer}", "opacity", '1', { fromValue: '0'}], position: 250, duration: 350 },
            { id: "eid154", tween: [ "style", "${_emailContainer}", "opacity", '0', { fromValue: '1'}], position: 750, duration: 250 }         ]
      }
   }
},
"modelBox": {
   version: "1.5.0",
   minimumCompatibleVersion: "1.5.0",
   build: "1.5.0.217",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      rect: ['1px','0px','685px','815px','auto','auto'],
      id: 'box',
      stroke: [0,'rgb(0, 0, 0)','none'],
      type: 'rect',
      fill: ['rgba(58,54,55,1.00)'],
      c: [
      {
         rect: ['0px','0px','685px','90px','auto','auto'],
         id: 'titleRect',
         stroke: [0,'rgb(0, 0, 0)','none'],
         type: 'rect',
         fill: ['rgba(181,18,27,1.00)'],
         c: [
         {
            rect: ['574px','9px','100px','72px','auto','auto'],
            borderRadius: ['10px','10px','10px','10px'],
            id: 'moreBox',
            stroke: [0,'rgb(0, 0, 0)','none'],
            type: 'rect',
            fill: ['rgba(255,255,255,1.00)'],
            c: [
            {
               font: ['\'Trade Gothic\'',30,'rgba(181,18,27,1.00)','normal','none','normal'],
               type: 'text',
               id: 'more',
               text: 'VIEW<br>HOME',
               align: 'center',
               rect: ['auto','auto','100px','72px','0px','0px']
            }]
         },
         {
            font: ['Trade Gothic',60,'rgba(255,255,255,1.00)','normal','none',''],
            type: 'text',
            id: 'title',
            text: 'BIRCH FALLS',
            align: 'center',
            rect: ['-1px','15px','685px','62px','auto','auto']
         }]
      },
      {
         rect: ['0px','695px','685px','120px','auto','auto'],
         id: 'infoRect',
         stroke: [0,'rgb(0, 0, 0)','none'],
         type: 'rect',
         fill: ['rgba(0,0,0,1.00)'],
         c: [
         {
            font: ['Trade Gothic',60,'rgba(255,255,255,1)','500','none','normal'],
            type: 'text',
            id: 'sqFt',
            text: '2206 sq.ft.',
            align: 'center',
            rect: ['44px','22px','300px','75px','auto','auto']
         },
         {
            font: ['Trade Gothic',28,'rgba(255,255,255,1)','normal','none','normal'],
            type: 'text',
            id: 'extra',
            text: '3 bedrooms<br>2.5 bathrooms<br>3 Car Tandem Garage',
            align: 'center',
            rect: ['344px','7px','325px','105px','auto','auto']
         }]
      },
      {
         rect: ['0px','91px','685px','600px','auto','auto'],
         id: 'image',
         stroke: [0,'rgb(0, 0, 0)','none'],
         type: 'rect',
         fill: ['rgba(58,54,55,1)']
      }]
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_sqFt}": [
            ["style", "top", '22px'],
            ["style", "font-size", '60px'],
            ["style", "text-align", 'center'],
            ["style", "font-weight", '500'],
            ["style", "height", '75px'],
            ["style", "font-family", 'Trade Gothic'],
            ["style", "left", '44px'],
            ["style", "width", '300px']
         ],
         "${_infoRect}": [
            ["style", "top", '695px'],
            ["style", "height", '120px'],
            ["color", "background-color", 'rgba(0,0,0,1.00)'],
            ["style", "left", '0px'],
            ["style", "width", '685px']
         ],
         "${_titleRect}": [
            ["style", "top", '0px'],
            ["style", "height", '90px'],
            ["style", "left", '0px'],
            ["color", "background-color", 'rgba(181,18,27,1.00)']
         ],
         "${_extra}": [
            ["style", "top", '7px'],
            ["style", "text-align", 'center'],
            ["style", "width", '325px'],
            ["style", "height", '105px'],
            ["style", "font-family", 'Trade Gothic'],
            ["style", "left", '344px'],
            ["style", "font-size", '28px']
         ],
         "${_box}": [
            ["style", "top", '0px'],
            ["style", "overflow", 'hidden'],
            ["style", "height", '815px'],
            ["color", "background-color", 'rgba(58,54,55,1.00)'],
            ["style", "left", '1px'],
            ["style", "width", '685px']
         ],
         "${symbolSelector}": [
            ["style", "height", '815px'],
            ["style", "width", '685px']
         ],
         "${_moreBox}": [
            ["color", "background-color", 'rgba(255,255,255,1.00)'],
            ["style", "top", '9px'],
            ["style", "bottom", 'auto'],
            ["style", "height", '72px'],
            ["style", "right", 'auto'],
            ["style", "left", '574px'],
            ["style", "width", '100px']
         ],
         "${_image}": [
            ["style", "height", '600px'],
            ["style", "top", '91px'],
            ["style", "left", '0px'],
            ["style", "width", '685px']
         ],
         "${_more}": [
            ["style", "bottom", '0px'],
            ["color", "color", 'rgba(181,18,27,1.00)'],
            ["style", "right", '0px'],
            ["style", "left", 'auto'],
            ["style", "font-size", '30px'],
            ["style", "top", 'auto'],
            ["style", "text-align", 'center'],
            ["style", "height", '72px'],
            ["style", "width", '100px']
         ],
         "${_title}": [
            ["style", "top", '15px'],
            ["style", "width", '685px'],
            ["style", "text-align", 'center'],
            ["color", "color", 'rgba(255,255,255,1.00)'],
            ["style", "height", '62px'],
            ["style", "font-family", 'Trade Gothic'],
            ["style", "left", '-1px'],
            ["style", "font-size", '60px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 0,
         autoPlay: false,
         timeline: [
         ]
      }
   }
},
"community": {
   version: "1.5.0",
   minimumCompatibleVersion: "1.5.0",
   build: "1.5.0.217",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      id: 'bg-community',
      type: 'image',
      rect: ['0px','0px','1920px','1080px','auto','auto'],
      fill: ['rgba(0,0,0,0)','images/bg-community.jpg','0px','0px']
   },
   {
      type: 'rect',
      id: 'backLink',
      stroke: [0,'rgb(0, 0, 0)','none'],
      rect: ['0px','0px','125px','121px','auto','auto'],
      fill: ['rgba(192,192,192,0)']
   },
   {
      rect: ['500px','901px','1208px','178px','auto','auto'],
      borderRadius: ['10px','10px','10px','10px'],
      id: 'communityVideo',
      stroke: [0,'rgb(0, 0, 0)','none'],
      type: 'rect',
      fill: ['rgba(184,171,162,0.00)']
   },
   {
      type: 'rect',
      borderRadius: ['10px','10px','10px','10px'],
      rect: ['699px','913px','779px','153px','auto','auto'],
      id: 'communityMapLink',
      stroke: [0,'rgb(0, 0, 0)','none'],
      display: 'none',
      fill: ['rgba(181,18,27,1.00)'],
      c: [
      {
         font: ['\'Trade Gothic\'',50,'rgba(255,255,255,1.00)','normal','none','normal'],
         type: 'text',
         id: 'communityText',
         text: 'Community Map &amp; Vision',
         align: 'center',
         rect: ['18px','46px','744px','60px','auto','auto']
      }]
   },
   {
      type: 'rect',
      rect: ['0px','0px','1920px','1080px','auto','auto'],
      id: 'videoPlayer',
      stroke: [0,'rgb(0, 0, 0)','none'],
      display: 'none',
      fill: ['rgba(255,255,255,0.00)']
   },
   {
      type: 'rect',
      rect: ['0px','0px','1920px','1079px','auto','auto'],
      id: 'communityCanvas',
      stroke: [0,'rgb(0, 0, 0)','none'],
      display: 'none',
      fill: ['rgba(255,255,255,1.00)'],
      c: [
      {
         id: 'community-map',
         type: 'image',
         rect: ['385px','40px','1150px','1000px','auto','auto'],
         fill: ['rgba(0,0,0,0)','images/community-map.png','0px','0px']
      },
      {
         type: 'rect',
         borderRadius: ['10px','10px','10px','10px'],
         rect: ['1617px','940px','270px','100px','auto','auto'],
         id: 'communityMapClose',
         stroke: [0,'rgb(0, 0, 0)','none'],
         display: 'none',
         fill: ['rgba(181,18,27,1.00)'],
         c: [
         {
            font: ['\'Trade Gothic\'',40,'rgba(255,255,255,1.00)','normal','none','normal'],
            type: 'text',
            id: 'communityClose',
            text: 'Close Map',
            align: 'center',
            rect: ['10px','25px','250px','50px','auto','auto']
         }]
      }]
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_communityCanvas}": [
            ["style", "top", '0px'],
            ["style", "height", '1079px'],
            ["style", "display", 'none'],
            ["style", "opacity", '0.000000'],
            ["color", "background-color", 'rgba(255,255,255,1.00)'],
            ["style", "width", '1920px']
         ],
         "${_bg-community}": [
            ["style", "left", '0px'],
            ["style", "top", '0px']
         ],
         "${_communityMapClose}": [
            ["style", "top", '940px'],
            ["style", "height", '100px'],
            ["color", "background-color", 'rgba(181,18,27,1.00)'],
            ["style", "display", 'none'],
            ["style", "opacity", '0.000000'],
            ["style", "left", '1617px'],
            ["style", "width", '270px']
         ],
         "${_communityVideo}": [
            ["color", "background-color", 'rgba(184,171,162,0.00)'],
            ["style", "height", '178px'],
            ["style", "top", '901px'],
            ["style", "left", '500px'],
            ["style", "width", '1208px']
         ],
         "${_communityMapLink}": [
            ["color", "background-color", 'rgba(181,18,27,1.00)'],
            ["style", "display", 'none'],
            ["style", "top", '913px'],
            ["style", "left", '699px'],
            ["style", "width", '779px']
         ],
         "${_communityClose}": [
            ["style", "top", '25px'],
            ["style", "width", '250px'],
            ["style", "text-align", 'center'],
            ["style", "height", '50px'],
            ["color", "color", 'rgba(255,255,255,1)'],
            ["style", "font-family", '\'Trade Gothic\''],
            ["style", "left", '10px'],
            ["style", "font-size", '40px']
         ],
         "${_community-map}": [
            ["style", "top", '40px'],
            ["style", "left", '385px'],
            ["style", "width", '1150px']
         ],
         "${_videoPlayer}": [
            ["style", "top", '0px'],
            ["style", "display", 'none'],
            ["style", "height", '1080px'],
            ["color", "background-color", 'rgba(0,0,0,1)'],
            ["style", "left", '0px'],
            ["style", "width", '1920px']
         ],
         "${_communityText}": [
            ["style", "top", '46px'],
            ["style", "font-size", '50px'],
            ["style", "text-align", 'center'],
            ["color", "color", 'rgba(255,255,255,1.00)'],
            ["style", "height", '60px'],
            ["style", "font-family", '\'Trade Gothic\''],
            ["style", "left", '18px'],
            ["style", "width", '744px']
         ],
         "${symbolSelector}": [
            ["style", "height", '1080px'],
            ["style", "width", '1920px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 1000,
         autoPlay: false,
         labels: {
            "fade-in-community": 250,
            "fade-out-community": 750
         },
         timeline: [
            { id: "eid157", tween: [ "style", "${_communityCanvas}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid160", tween: [ "style", "${_communityCanvas}", "display", 'block', { fromValue: 'none'}], position: 250, duration: 0 },
            { id: "eid229", tween: [ "style", "${_communityMapLink}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid158", tween: [ "style", "${_communityCanvas}", "opacity", '1', { fromValue: '0.000000'}], position: 250, duration: 250 },
            { id: "eid159", tween: [ "style", "${_communityCanvas}", "opacity", '0', { fromValue: '1'}], position: 750, duration: 250 },
            { id: "eid164", tween: [ "style", "${_videoPlayer}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid208", tween: [ "style", "${_communityMapClose}", "opacity", '1', { fromValue: '0.000000'}], position: 250, duration: 250 },
            { id: "eid209", tween: [ "style", "${_communityMapClose}", "opacity", '0', { fromValue: '1'}], position: 750, duration: 250 },
            { id: "eid207", tween: [ "style", "${_communityMapClose}", "display", 'block', { fromValue: 'none'}], position: 250, duration: 0 },
            { id: "eid210", tween: [ "style", "${_communityMapClose}", "display", 'none', { fromValue: 'block'}], position: 1000, duration: 0 }         ]
      }
   }
},
"emailContainer": {
   version: "1.5.0",
   minimumCompatibleVersion: "1.5.0",
   build: "1.5.0.217",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      rect: ['0px','0px','1917px','979px','auto','auto'],
      id: 'emailContainer',
      stroke: [0,'rgb(0, 0, 0)','none'],
      type: 'rect',
      fill: ['rgba(10,10,10,0.90)']
   },
   {
      rect: ['50px','110px','1352px','867px','auto','auto'],
      id: 'modelBox',
      stroke: [0,'rgb(0, 0, 0)','none'],
      type: 'rect',
      fill: ['rgba(58,54,55,0.00)']
   },
   {
      font: ['Trade Gothic',70,'rgba(255,255,255,1.00)','normal','none','normal'],
      type: 'text',
      id: 'title',
      text: 'Select Brochures to Email:',
      align: 'left',
      rect: ['50px','20px','1000px','80px','auto','auto']
   },
   {
      rect: ['1415px','0px','565px','970px','auto','auto'],
      id: 'emailForm',
      stroke: [0,'rgb(0, 0, 0)','none'],
      type: 'rect',
      fill: ['rgba(58,54,55,0)']
   },
   {
      rect: ['1439px','519px','399px','86px','auto','auto'],
      borderRadius: ['10px','10px','10px','10px'],
      id: 'sendBox',
      stroke: [0,'rgb(0, 0, 0)','none'],
      type: 'rect',
      fill: ['rgba(181,18,27,1.00)'],
      c: [
      {
         font: ['Trade Gothic',52,'rgba(255,255,255,1.00)','normal','none','normal'],
         type: 'text',
         id: 'sendText',
         text: 'Send Email',
         align: 'center',
         rect: ['66px','15px','268px','55px','auto','auto']
      }]
   },
   {
      rect: ['1670px','830px','200px','100px','auto','auto'],
      borderRadius: ['10px','10px','10px','10px'],
      id: 'emailClose',
      stroke: [0,'rgb(0, 0, 0)','none'],
      type: 'rect',
      fill: ['rgba(0,0,0,1.00)'],
      c: [
      {
         font: ['Trade Gothic',50,'rgba(255,255,255,1.00)','normal','none',''],
         type: 'text',
         id: 'emailCloseText',
         text: 'CLOSE',
         align: 'center',
         rect: ['25px','20px','150px','60px','auto','auto']
      }]
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_emailContainer}": [
            ["style", "top", '0px'],
            ["style", "height", '979px'],
            ["color", "background-color", 'rgba(10,10,10,0.90)'],
            ["style", "left", '0px'],
            ["style", "width", '1917px']
         ],
         "${_modelBox}": [
            ["style", "height", '867px'],
            ["color", "background-color", 'rgba(58,54,55,0.00)'],
            ["style", "left", '50px'],
            ["style", "top", '110px']
         ],
         "${_emailClose}": [
            ["style", "top", '830px'],
            ["style", "height", '100px'],
            ["color", "background-color", 'rgba(0,0,0,1)'],
            ["style", "left", '1670px'],
            ["style", "width", '200px']
         ],
         "${_sendBox}": [
            ["color", "background-color", 'rgba(181,18,27,1.00)'],
            ["style", "top", '519px'],
            ["style", "left", '1439px'],
            ["style", "width", '399px']
         ],
         "${_emailForm}": [
            ["style", "height", '970px'],
            ["style", "left", '1415px']
         ],
         "${_sendText}": [
            ["style", "top", '15px'],
            ["style", "text-align", 'center'],
            ["color", "color", 'rgba(255,255,255,1.00)'],
            ["style", "height", '55px'],
            ["style", "font-family", 'Trade Gothic'],
            ["style", "left", '66px'],
            ["style", "width", '268px']
         ],
         "${symbolSelector}": [
            ["style", "height", '977px'],
            ["style", "width", '1920px']
         ],
         "${_emailCloseText}": [
            ["style", "top", '20px'],
            ["style", "font-size", '50px'],
            ["style", "text-align", 'center'],
            ["style", "height", '60px'],
            ["color", "color", 'rgba(255,255,255,1.00)'],
            ["style", "font-family", '\'Trade Gothic\''],
            ["style", "left", '25px'],
            ["style", "width", '150px']
         ],
         "${_title}": [
            ["style", "top", '20px'],
            ["style", "font-size", '70px'],
            ["color", "color", 'rgba(255,255,255,1.00)'],
            ["style", "height", '80px'],
            ["style", "font-family", 'Trade Gothic'],
            ["style", "left", '50px'],
            ["style", "width", '1000px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 0,
         autoPlay: false,
         timeline: [
         ]
      }
   }
},
"galleryContainer": {
   version: "1.5.0",
   minimumCompatibleVersion: "1.5.0",
   build: "1.5.0.217",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      rect: ['0px','0px','1920px','980px','auto','auto'],
      stroke: [0,'rgba(0,0,0,1)','none'],
      id: 'galleryContainer',
      opacity: 1,
      type: 'rect',
      fill: ['rgba(10,10,10,0.90)']
   },
   {
      rect: ['511px','40px','900px','900px','auto','auto'],
      borderRadius: ['10px 10px','10px 10px','10px 10px','10px 10px'],
      id: 'whiteBox',
      stroke: [0,'rgb(0, 0, 0)','none'],
      type: 'rect',
      fill: ['rgba(219,210,206,1.00)']
   },
   {
      rect: ['529px','60px','860px','860px','auto','auto'],
      id: 'galleryHolderCopy',
      stroke: [0,'rgb(0, 0, 0)','none'],
      type: 'rect',
      fill: ['rgba(255,255,255,0.00)'],
      c: [
      {
         rect: ['0px','0px','200px','200px','auto','auto'],
         id: 'gallery1',
         stroke: [0,'rgb(0, 0, 0)','none'],
         type: 'rect',
         fill: ['rgba(183,169,164,1.00)']
      },
      {
         rect: ['220px','0px','200px','200px','auto','auto'],
         id: 'gallery2',
         stroke: [0,'rgb(0, 0, 0)','none'],
         type: 'rect',
         fill: ['rgba(183,169,164,1.00)']
      },
      {
         rect: ['440px','0px','200px','200px','auto','auto'],
         id: 'gallery3',
         stroke: [0,'rgb(0, 0, 0)','none'],
         type: 'rect',
         fill: ['rgba(183,169,164,1.00)']
      },
      {
         rect: ['660px','0px','200px','200px','auto','auto'],
         id: 'gallery4',
         stroke: [0,'rgb(0, 0, 0)','none'],
         type: 'rect',
         fill: ['rgba(183,169,164,1.00)']
      },
      {
         rect: ['0px','220px','200px','200px','auto','auto'],
         id: 'gallery5',
         stroke: [0,'rgb(0, 0, 0)','none'],
         type: 'rect',
         fill: ['rgba(183,169,164,1.00)']
      },
      {
         rect: ['220px','220px','200px','200px','auto','auto'],
         id: 'gallery6',
         stroke: [0,'rgb(0, 0, 0)','none'],
         type: 'rect',
         fill: ['rgba(183,169,164,1.00)']
      },
      {
         rect: ['440px','220px','200px','200px','auto','auto'],
         id: 'gallery7',
         stroke: [0,'rgb(0, 0, 0)','none'],
         type: 'rect',
         fill: ['rgba(183,169,164,1.00)']
      },
      {
         rect: ['660px','220px','200px','200px','auto','auto'],
         id: 'gallery8',
         stroke: [0,'rgb(0, 0, 0)','none'],
         type: 'rect',
         fill: ['rgba(183,169,164,1.00)']
      },
      {
         rect: ['0px','440px','200px','200px','auto','auto'],
         id: 'gallery9',
         stroke: [0,'rgb(0, 0, 0)','none'],
         type: 'rect',
         fill: ['rgba(183,169,164,1.00)']
      },
      {
         rect: ['220px','440px','200px','200px','auto','auto'],
         id: 'gallery10',
         stroke: [0,'rgb(0, 0, 0)','none'],
         type: 'rect',
         fill: ['rgba(183,169,164,1.00)']
      },
      {
         rect: ['440px','440px','200px','200px','auto','auto'],
         id: 'gallery11',
         stroke: [0,'rgb(0, 0, 0)','none'],
         type: 'rect',
         fill: ['rgba(183,169,164,1.00)']
      },
      {
         rect: ['660px','440px','200px','200px','auto','auto'],
         id: 'gallery12',
         stroke: [0,'rgb(0, 0, 0)','none'],
         type: 'rect',
         fill: ['rgba(183,169,164,1.00)']
      },
      {
         rect: ['0px','660px','200px','200px','auto','auto'],
         id: 'gallery13',
         stroke: [0,'rgb(0, 0, 0)','none'],
         type: 'rect',
         fill: ['rgba(183,169,164,1.00)']
      },
      {
         rect: ['220px','660px','200px','200px','auto','auto'],
         id: 'gallery14',
         stroke: [0,'rgb(0, 0, 0)','none'],
         type: 'rect',
         fill: ['rgba(183,169,164,1.00)']
      },
      {
         rect: ['440px','660px','200px','200px','auto','auto'],
         id: 'gallery15',
         stroke: [0,'rgb(0, 0, 0)','none'],
         type: 'rect',
         fill: ['rgba(183,169,164,1.00)']
      },
      {
         rect: ['660px','660px','200px','200px','auto','auto'],
         id: 'gallery16',
         stroke: [0,'rgb(0, 0, 0)','none'],
         type: 'rect',
         fill: ['rgba(183,169,164,1.00)']
      }]
   },
   {
      rect: ['1670px','830px','200px','100px','auto','auto'],
      borderRadius: ['10px','10px','10px','10px'],
      id: 'galleryClose',
      stroke: [0,'rgb(0, 0, 0)','none'],
      type: 'rect',
      fill: ['rgba(0,0,0,1.00)'],
      c: [
      {
         font: ['Trade Gothic',50,'rgba(219,210,206,1.00)','normal','none',''],
         type: 'text',
         id: 'galleryCloseText',
         text: 'CLOSE',
         align: 'center',
         rect: ['25px','20px','150px','60px','auto','auto']
      }]
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_gallery1}": [
            ["style", "top", '0px'],
            ["style", "display", 'block'],
            ["style", "height", '200px'],
            ["color", "background-color", 'rgba(183,169,164,1.00)'],
            ["style", "left", '0px'],
            ["style", "width", '200px']
         ],
         "${_gallery2}": [
            ["style", "top", '0px'],
            ["style", "display", 'block'],
            ["style", "height", '200px'],
            ["color", "background-color", 'rgba(183,169,164,1.00)'],
            ["style", "left", '220px'],
            ["style", "width", '200px']
         ],
         "${_gallery13}": [
            ["style", "top", '660px'],
            ["style", "display", 'block'],
            ["style", "height", '200px'],
            ["color", "background-color", 'rgba(183,169,164,1.00)'],
            ["style", "left", '0px'],
            ["style", "width", '200px']
         ],
         "${_whiteBox}": [
            ["color", "background-color", 'rgba(219,210,206,1.00)'],
            ["style", "border-top-left-radius", [10,10], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "border-bottom-right-radius", [10,10], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "border-top-right-radius", [10,10], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "width", '900px'],
            ["style", "top", '40px'],
            ["style", "border-bottom-left-radius", [10,10], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "height", '900px'],
            ["style", "left", '511px']
         ],
         "${symbolSelector}": [
            ["style", "height", '980px'],
            ["style", "width", '1922px']
         ],
         "${_gallery6}": [
            ["style", "top", '220px'],
            ["style", "display", 'block'],
            ["style", "height", '200px'],
            ["color", "background-color", 'rgba(183,169,164,1.00)'],
            ["style", "left", '220px'],
            ["style", "width", '200px']
         ],
         "${_gallery16}": [
            ["style", "top", '660px'],
            ["style", "display", 'block'],
            ["style", "height", '200px'],
            ["color", "background-color", 'rgba(183,169,164,1.00)'],
            ["style", "left", '660px'],
            ["style", "width", '200px']
         ],
         "${_gallery10}": [
            ["style", "top", '440px'],
            ["style", "display", 'block'],
            ["style", "height", '200px'],
            ["color", "background-color", 'rgba(183,169,164,1.00)'],
            ["style", "left", '220px'],
            ["style", "width", '200px']
         ],
         "${_gallery5}": [
            ["style", "top", '220px'],
            ["style", "display", 'block'],
            ["style", "height", '200px'],
            ["color", "background-color", 'rgba(183,169,164,1.00)'],
            ["style", "left", '0px'],
            ["style", "width", '200px']
         ],
         "${_galleryHolderCopy}": [
            ["color", "background-color", 'rgba(255,255,255,0.00)'],
            ["style", "height", '860px'],
            ["style", "width", '860px']
         ],
         "${_gallery8}": [
            ["style", "top", '220px'],
            ["style", "display", 'block'],
            ["style", "height", '200px'],
            ["color", "background-color", 'rgba(183,169,164,1.00)'],
            ["style", "left", '660px'],
            ["style", "width", '200px']
         ],
         "${_gallery4}": [
            ["style", "top", '0px'],
            ["style", "display", 'block'],
            ["style", "height", '200px'],
            ["color", "background-color", 'rgba(183,169,164,1.00)'],
            ["style", "left", '660px'],
            ["style", "width", '200px']
         ],
         "${_gallery9}": [
            ["style", "top", '440px'],
            ["style", "display", 'block'],
            ["style", "height", '200px'],
            ["color", "background-color", 'rgba(183,169,164,1.00)'],
            ["style", "left", '0px'],
            ["style", "width", '200px']
         ],
         "${_gallery3}": [
            ["style", "top", '0px'],
            ["style", "display", 'block'],
            ["style", "height", '200px'],
            ["color", "background-color", 'rgba(183,169,164,1.00)'],
            ["style", "left", '440px'],
            ["style", "width", '200px']
         ],
         "${_galleryClose}": [
            ["style", "top", '830px'],
            ["style", "height", '100px'],
            ["color", "background-color", 'rgba(0,0,0,1.00)'],
            ["style", "left", '1670px'],
            ["style", "width", '200px']
         ],
         "${_gallery15}": [
            ["style", "top", '660px'],
            ["style", "display", 'block'],
            ["style", "height", '200px'],
            ["color", "background-color", 'rgba(183,169,164,1.00)'],
            ["style", "left", '440px'],
            ["style", "width", '200px']
         ],
         "${_galleryContainer}": [
            ["style", "top", '0px'],
            ["style", "height", '980px'],
            ["style", "opacity", '1'],
            ["style", "left", '0px'],
            ["color", "background-color", 'rgba(10,10,10,0.90)']
         ],
         "${_gallery12}": [
            ["style", "top", '440px'],
            ["style", "display", 'block'],
            ["style", "height", '200px'],
            ["color", "background-color", 'rgba(183,169,164,1.00)'],
            ["style", "left", '660px'],
            ["style", "width", '200px']
         ],
         "${_gallery11}": [
            ["style", "top", '440px'],
            ["style", "display", 'block'],
            ["style", "height", '200px'],
            ["color", "background-color", 'rgba(183,169,164,1.00)'],
            ["style", "left", '440px'],
            ["style", "width", '200px']
         ],
         "${_galleryCloseText}": [
            ["style", "top", '20px'],
            ["style", "width", '150px'],
            ["style", "text-align", 'center'],
            ["color", "color", 'rgba(219,210,206,1.00)'],
            ["style", "height", '60px'],
            ["style", "font-family", 'Trade Gothic'],
            ["style", "left", '25px'],
            ["style", "font-size", '50px']
         ],
         "${_gallery14}": [
            ["style", "top", '660px'],
            ["style", "display", 'block'],
            ["style", "height", '200px'],
            ["color", "background-color", 'rgba(183,169,164,1.00)'],
            ["style", "left", '220px'],
            ["style", "width", '200px']
         ],
         "${_gallery7}": [
            ["style", "top", '220px'],
            ["style", "display", 'block'],
            ["style", "height", '200px'],
            ["color", "background-color", 'rgba(183,169,164,1.00)'],
            ["style", "left", '440px'],
            ["style", "width", '200px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 0,
         autoPlay: false,
         timeline: [
            { id: "eid183", tween: [ "style", "${_gallery4}", "display", 'block', { fromValue: 'block'}], position: 0, duration: 0 },
            { id: "eid195", tween: [ "style", "${_gallery16}", "display", 'block', { fromValue: 'block'}], position: 0, duration: 0 },
            { id: "eid188", tween: [ "style", "${_gallery9}", "display", 'block', { fromValue: 'block'}], position: 0, duration: 0 },
            { id: "eid182", tween: [ "style", "${_gallery3}", "display", 'block', { fromValue: 'block'}], position: 0, duration: 0 },
            { id: "eid181", tween: [ "style", "${_gallery2}", "display", 'block', { fromValue: 'block'}], position: 0, duration: 0 },
            { id: "eid180", tween: [ "style", "${_gallery1}", "display", 'block', { fromValue: 'block'}], position: 0, duration: 0 },
            { id: "eid193", tween: [ "style", "${_gallery15}", "display", 'block', { fromValue: 'block'}], position: 0, duration: 0 },
            { id: "eid185", tween: [ "style", "${_gallery6}", "display", 'block', { fromValue: 'block'}], position: 0, duration: 0 },
            { id: "eid186", tween: [ "style", "${_gallery7}", "display", 'block', { fromValue: 'block'}], position: 0, duration: 0 },
            { id: "eid189", tween: [ "style", "${_gallery13}", "display", 'block', { fromValue: 'block'}], position: 0, duration: 0 },
            { id: "eid190", tween: [ "style", "${_gallery10}", "display", 'block', { fromValue: 'block'}], position: 0, duration: 0 },
            { id: "eid191", tween: [ "style", "${_gallery14}", "display", 'block', { fromValue: 'block'}], position: 0, duration: 0 },
            { id: "eid194", tween: [ "style", "${_gallery12}", "display", 'block', { fromValue: 'block'}], position: 0, duration: 0 },
            { id: "eid184", tween: [ "style", "${_gallery5}", "display", 'block', { fromValue: 'block'}], position: 0, duration: 0 },
            { id: "eid187", tween: [ "style", "${_gallery8}", "display", 'block', { fromValue: 'block'}], position: 0, duration: 0 },
            { id: "eid192", tween: [ "style", "${_gallery11}", "display", 'block', { fromValue: 'block'}], position: 0, duration: 0 }         ]
      }
   }
}
};


Edge.registerCompositionDefn(compId, symbols, fonts, resources);

/**
 * Adobe Edge DOM Ready Event Handler
 */
$(window).ready(function() {
     Edge.launchComposition(compId);
});
})(jQuery, AdobeEdge, "EDGE-KINNIBURGH");
